﻿using WindowsFormsApp1.Properties;
using System;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    static class Program
    {
        
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string macAddress1 = Settings.Default["macAddress"].ToString();
            string macAddress2 = GetMACAddress();
            bool provNaSpasib = Convert.ToBoolean(Settings.Default["provNaSpasib"]);

            //если на этом компьютере уже запускали эту прогу + ставили спасибо то пропускаем пункт со спасибкой
            if (macAddress1 != macAddress2 || provNaSpasib != true)
            {
                Application.Run(new Chrome());
            }

            string macAddress1_ = Settings.Default["macAddress"].ToString();
            bool provNaSpasib_ = Convert.ToBoolean(Settings.Default["provNaSpasib"]);

            
            if (macAddress1_ == macAddress2 && provNaSpasib_ == true)
            {
                 Application.Run(new Form1()); 
            }
            

        }

        static public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            return sMacAddress;
        }
    }
}

