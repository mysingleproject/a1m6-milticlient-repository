﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Properties;

namespace WindowsFormsApp1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            try
            {
                textBox1.Text = Settings.Default["path"].ToString();
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Settings.Default["path"] = textBox1.Text;
            Settings.Default.Save();
            Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }

}

