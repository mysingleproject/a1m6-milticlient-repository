﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
  

    public partial class Add : Form
    {
        internal Account newacc = new Account();
        public Add()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            if (textBox2.Text.Length != 32)
            {
                byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(textBox2.Text));
                newacc = new Account(textBox1.Text, BitConverter.ToString(checkSum1).Replace("-", String.Empty), textBox3.Text);
            }
            else
            {
                newacc = new Account(textBox1.Text, textBox2.Text, textBox3.Text);
                
            }
            StatClass.statacc.uid = newacc.uid;
            StatClass.statacc.tokken = newacc.tokken;
            StatClass.statacc.comment = newacc.comment;
            Close();

        }
        
    }
    public static class StatClass
    {
        //Данная переменная статического класса будет доступна откуда угодно в пределах проекта
        public static Account statacc = new Account();
    }
}
